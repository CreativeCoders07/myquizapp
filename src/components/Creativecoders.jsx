import { useEffect, useState } from "react"
import useSound from "use-sound";
import play from '../sounds/src_sounds_play.mp3';
import correct from '../sounds/src_sounds_correct.mp3';
import wrong from '../sounds/src_sounds_wrong.mp3';





export default function Creativecoders({ data, setStop, quesNum, setQuesNum }) {

    const [question, setQuestion] = useState(null)
    const [selectedAnswer, setselectedAnswer] = useState(null)
    const [className, setclassName] = useState('answer')
    const [letsplay] = useSound(play);
    const [correctAnswer] = useSound(correct);
    const [wrongAnswer] = useSound(wrong);
    
        useEffect(()=>{
            letsplay();
        },[letsplay]);
    useEffect(() => {
        debugger
        console.log(data);
        const detail = data[quesNum - 1];
        setQuestion(detail);
        console.log(question, "dasdasdsad");
    }, [data, quesNum]);

    const delay = (duration, callback) => {
        setTimeout(() => {
            callback()
        }, duration)
    }

    const handleClick = (a) => {
        setselectedAnswer(a)
        setclassName('answer active')
        delay(3000, () => setclassName(a.correct ? ' answer correct' : 'answer wrong'));
        delay(5000,()=>{
            if(a.correct){
                correctAnswer()
                delay(1000,()=>{

                    setQuesNum((prev)=>prev+1)
                    setselectedAnswer(null)
                });
            }else{
                wrongAnswer()
                delay(1000,()=>{

                    setStop(true)
                })
            }
        })

    }
    return (
        <div className="cc">
            <div className="questions"> {question?.question}</div>
            <div className="answers ">
                {question?.answers.map((a) => (


                    <div className={selectedAnswer === a ? className : "answer"} onClick={() => handleClick(a)}>{a.text}</div>
                ))}
                {/* <div className="answer">CreaticeCoders</div>
              <div className="answer">thoughtworks</div>
              <div className="answer">zoho</div> */}

            </div>

        </div>
    )
}
