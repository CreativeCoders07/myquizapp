import { useState } from "react/cjs/react.development"

export default function Start({setUserName}) {
    const inputRef = useState();
    const handleClick = () => {
        inputRef.current.value && setUserName(inputRef.current.value)
    };
    
  return (
      <div className="start">
          <input placeholder="Enter youName" className="startInput" ref={inputRef}/>
          <button className="startButton" onClick={handleClick}>Start</button>

         

      </div>
  )
}
