export  const data = [
    {
      id: 1,
      question: "What is the nick Name of varsha?",
      answers: [
        {
          text: "Partiality",
          correct: false,
        },
        {
          text: "Sakthisister",
          correct: true,
        },
        {
          text: "Possasive",
          correct: false,
        },
        {
          text: "mendal",
          correct: false,
        },
      ],
    },
    {
      id: 2,
      question: "What is the name of keerthana lover?",
      answers: [
        {
          text: "kalai",
          correct: true,
        },
        {
          text: "siva",
          correct: false,
        },
        {
          text: "dio guy",
          correct: false,
        },
        {
          text: "insta guy",
          correct: false,
        },
      ],
    },
    {
      id: 3,
      question: "Who is the lover of sakthi?",
      answers: [
        {
          text: "abirami",
          correct: false,
        },
        {
          text: "varsha",
          correct: false,
        },
        {
          text: "keerthana",
          correct: false,
        },
        {
          text: "sujitha",
          correct: true,
        },
      ],
    },
    {
      id: 4,
      question: "Who is the jayaram girlfriend?",
      answers: [
        {
          text: "Divya Shree",
          correct: false,
        },
        {
          text: "Priya",
          correct: false,
        },
        {
          text: "Shuhasini",
          correct: false,
        },
        {
          text: "Abirami",
          correct: true,
        },
      ],
    },
    {
      id: 5,
      question: "What is the nick Name of keerthana?",
      answers: [
        {
          text: "Kali lover",
          correct: false,
        },
        {
          text: "paithiyam",
          correct: false,
        },
        {
          text: "patient",
          correct: false,
        },
        {
          text: "TheeyaSakthi",
          correct: true,
        },
      ],
    },
    {
      id: 6,
      question: "what is the nick Name of Annamalai?",
      answers: [
        {
          text: "genie",
          correct: true,
        },
        {
          text: "playboy",
          correct: false,
        },
        {
          text: "partiality",
          correct: false,
        },
        {
          text: "possasive",
          correct: false,
        },
      ],
    },
    {
      id: 7,
      question: "Who is the girlFriend of Annamalai?",
      answers: [
        {
          text: "Ravali",
          correct: false,
        },
        {
          text: "SriJan",
          correct: false,
        },
        {
          text: "miss X",
          correct: true,
        },
        {
          text: "Ammu",
          correct: false,
        },
      ],
    },
    {
        id: 8,
        question: "Who is best frend of varsh?",
        answers: [
          {
            text: "Sakthi",
            correct: false,
          },
          {
            text: "jeevan",
            correct: true,
          },
          {
            text: "Annamalai",
            correct: false,
          },
          {
            text: "Jayaram",
            correct: true,
          },
        ],
      },
      {
        id: 9,
        question: "what is the name of our PodOwner?",
        answers: [
          {
            text: "Nandy",
            correct: true,
          },
          {
            text: "muthu",
            correct: false,
          },
          {
            text: "febin",
            correct: false,
          },
          {
            text: "Pritam",
            correct: false,
          },
        ],
      },
      {
        id: 10,
        question: "how many amount balance varsha whants to given ?",
        answers: [
          {
            text: "0 Rs",
            correct: true,
          },
          {
            text: "2452",
            correct: false,
          },
          {
            text: "1900",
            correct: false,
          },
          {
            text: "4252",
            correct: false,
          },
        ],
      },
      {
        id: 11,
        question: "how many amount balance keerthana whants to given?",
        answers: [
          {
            text: "400",
            correct: false,
          },
          {
            text: "300",
            correct: false,
          },
          {
            text: "600",
            correct: true,
          },
          {
            text: "100",
            correct: false,
          },
        ],
      },
      {
        id: 12,
        question: "what is the place keerthane going to given the treat?",
        answers: [
          {
            text: "start briyani",
            correct: false,
          },
          {
            text: "borader ahemad",
            correct: true,
          },
          {
            text: "Ashif",
            correct: false,
          },
          {
            text: "kannapa restarunt",
            correct: false,
          },
        ],
      },
      {
        id: 13,
        question: "What is the luchTime for our team?",
        answers: [
          {
            text: "1 pm",
            correct: false,
          },
          {
            text: "2pm",
            correct: false,
          },
          {
            text: "3 pm",
            correct: true,
          },
          {
            text: "vidinchurum",
            correct: false,
          },
        ],
      },
      {
        id: 14,
        question: " who is the lover of varsha?",
        answers: [
          {
            text: "Sakthi",
            correct: false,
          },
          {
            text: "Jeevan",
            correct: false,
          },
          {
            text: "Saran",
            correct: true,
          },
          {
            text: "Rahul",
            correct: false,
          },
        ],
      },
      {
        id: 15,
        question: "What is the Fav snacks for varsha and keerthana?",
        answers: [
          {
            text: "Fingerchips",
            correct: false,
          },
          {
            text: "lalipop",
            correct: false,
          },
          {
            text: "sandwich",
            correct: false,
          },
          {
            text: "All of them",
            correct: true,
          },
        ],
      },
  ];
  export default data;